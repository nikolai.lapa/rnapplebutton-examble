//
//  RNAppleWalletManager.m
//  client
//
//  Created by Nikolay Lapa on 16.05.22.
//

#import "RNAppleWalletManager.h"
#import "RNAppleButtonView.h"

@implementation RNAppleWalletManager

RCT_EXPORT_MODULE(RNAppleButton)
RCT_EXPORT_VIEW_PROPERTY(onPress, RCTBubblingEventBlock)


- (UIView *)view
{
    RNAppleButtonView *button = [[RNAppleButtonView alloc] initWithAddPassButtonStyle:PKAddPassButtonStyleBlack];
    return button;
}

@end
