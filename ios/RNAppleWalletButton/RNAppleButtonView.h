//
//  RNAppleButtonView.h
//  client
//
//  Created by Nikolay Lapa on 16.05.22.
//

#import <PassKit/PassKit.h>
#import <React/RCTComponent.h>

@interface RNAppleButtonView : UIView

- (instancetype)initWithAddPassButtonStyle:(PKAddPassButtonStyle)style;

@property (nonatomic, retain) PKAddPassButton *addPassButton;
@property (nonatomic, copy) RCTBubblingEventBlock onPress;

@end
