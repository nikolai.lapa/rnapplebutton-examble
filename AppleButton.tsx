import React, {VFC} from 'react';
import {
  StyleSheet,
  View,
  Text,
  requireNativeComponent,
  useWindowDimensions,
  ViewProps,
} from 'react-native';
import {RNAppleButton} from './NativeButton';

const styles = StyleSheet.create({
  button: {height: 56},
});

type AppleButtonProps = {onPress: () => void; style?: ViewProps['style']};

const AppleButton: VFC<AppleButtonProps> = ({onPress, style}) => {
  return <RNAppleButton onPress={onPress} style={[styles.button, style]} />;
};

export {AppleButton};
