import {requireNativeComponent, ViewProps} from 'react-native';

type AppleButtonProps = {onPress: () => void; style?: ViewProps['style']};

export const RNAppleButton =
  requireNativeComponent<AppleButtonProps>('RNAppleButton');
